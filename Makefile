build_dir=build
man3_dir=$(build_dir)/man/man3
install_dir=/usr/local/man/man3/


man:
	doxygen Doxyfile


.PHONY: clean
clean:
	rm -r build


.PHONY: install
install:
	gzip $(man3_dir)/SDL_*
	mkdir -p "$(install_dir)"
	cp $(man3_dir)/SDL_*.gz "$(install_dir)"
